const fastify = require('fastify')({ logger: { level: 'error' } });

const natsServer = process.env.server || 'localhost';
const natsUser = process.env.user || 'user';
const natsPass = process.env.pass || 'pass';

fastify.register(require('fastify-nats'), {
  url: `nats://${natsUser}:${natsPass}@${natsServer}:4222`
}, err => {
  if (err) throw err
});

fastify.get('/', function (request, reply) {
  fastify.nats.publish('request', JSON.stringify({url: request.raw.url, method: request.raw.method}));
  reply.send({ message: 'received the request and triggered a message' });
});
 
fastify.listen(4000, '0.0.0.0', (err) => {
  if (err) throw err;
})