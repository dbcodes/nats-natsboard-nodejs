const nats = require('./natsClient');
const logger = require('roarr').default;

const interval = process.env.interval || 1000;

nats.subscribe('result', (msg) => {
  let task = tasks[msg.index];
  logger.info(`The result from ${task.a} + ${task.b} is: ${msg.result}`);
});

nats.subscribe('error.*', (msg) => {
  logger.info(`Error happens: ${msg.message}`);
});

let tasks = [
  {a: 3, b: 8},
  {a: -2, b: 40},
  {a: -2, b: -5},
  {a: 300, b: 40.50},
  {a: 0.8908, b: 0.345},
];

setInterval(() => {
  let task = tasks[Math.floor(Math.random()*tasks.length)];
  let taskIndex = tasks.findIndex(x => task === x);
  task.index = taskIndex;
  nats.publish('compute', task, 'result');
}, interval);