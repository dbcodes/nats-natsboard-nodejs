const nats = require('./natsClient');
const logger = require('roarr').default;

const interval = process.env.interval || 1000;

const subjects = ['log', 'error.notice', 'error.urgent'];
const msgs = {
  log: [
    { content: 'Service available' },
    { content: 'Service ready' },
    { content: 'Service connect' },
  ],
  error: [
    { message: 'Lost connection' },
    { message: 'Resource limit' },
    { message: 'Out of memory' },
  ],
};

setInterval(() => {
  subjects.forEach((subject) => {
    let sub = subject.split('.')[0];
    let msg = msgs[sub][Math.floor(Math.random()*msgs[sub].length)];
    nats.publish(subject, msg, () => {
      logger.info(`Send to ${subject}`);
    });
  });
}, interval);
