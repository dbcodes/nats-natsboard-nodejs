const logger = require('roarr').default;
const NATS = require('nats');

const natsServer = process.env.server || 'localhost';
const natsUser = process.env.user || 'user';
const natsPass = process.env.pass || 'pass';

const nats = NATS.connect({
  url: `nats://${natsUser}:${natsPass}@${natsServer}:4222`,
  json: true
});

nats.on('error', (err) => {
  logger.error(err.message);
});

nats.on('connect', (nc) => {
  logger.info('connected');
});

nats.on('disconnect', () => {
  logger.error('disconnected');
});

nats.on('reconnecting', () => {
  logger.info('reconnecting');
});

nats.on('reconnect', (nc) => {
  logger.info('reconnected');
});

nats.on('close', () => {
  logger.error('closed');
});

module.exports = nats;