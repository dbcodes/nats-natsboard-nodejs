const nats = require('./natsClient');
const logger = require('roarr').default;

let sids = [];

/**
 * Object which holds handler for specific topics.  
 * Every handler can have the following params: msg, reply, subject
 */
const subjectHandler = {
  'log': (msg) => {
    logger.info(msg.content);
  },
  'compute': (msg, reply) => {
    logger.info(`Received task with ${msg.a} and ${msg.b} to compute.`);
    nats.publish(reply, {index: msg.index, result: msg.a + msg.b});
  },
  'request': (msg) => {
    logger.info(`Incoming request ${msg.method} ${msg.url}`);
  },
  'error.*': (msg) => {
    logger.error(msg.message);
  }
}
/**
 * Iterate over the subjects and subscribe for a subject with an handler.
 */
for (let subject in subjectHandler) {
  sids.push(nats.subscribe(subject, subjectHandler[subject]));
}

logger.info(`Start consuming from ${sids.length} subjects with ids ${sids}`);