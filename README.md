# nats-natsboard-nodejs

This example runs NATS (messaging system) and the natsboard (monitoring dashboard).  
Some small NodeJS applications interacting with NATS to produce and consume messages.  
It's an example to use NATS as communication layer in a microservice architecture.  

The consumer subscribe to several subjects and log the arriving messages to the console, one subject additional reply.  
The producer publish random messages in an interval to several subjects.  
The processor publish random tasks in an interval and subscribe for the results, coming from the consumer.  
The server is a very simple echo server which triggers a message for every request.  

# Run it
Run the complete example and see the console output.

    $ docker-compose up --build

You can browse the monitoring dashboard under this URL.

    http://localhost:3000/

The web server response under this URL.

    http://localhost:4000/

# Play around
Run only the communication layer.

    $ docker-compose up nats natsboard

You can then start the node applications local.

    $ cd app
    $ npm run consumer
    $ npm run producer
    $ npm run processor
    $ npm run server

## Feedback
Star this repo if you found it useful. Use the github issue tracker to give feedback on this repo.